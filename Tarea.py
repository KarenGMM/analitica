# Utilizando el modulo random y la funcion randint()
# Crear un programa que genere una lista de 10 numeros aleatorios, si la suma de los elementos es menor a 50 
# imprimir Eureka  sino  imprimir "este es el ultimo taller"
import numpy as np

# Generate 10 random data points 
x = np.random.randint(0,10,10)
print(x)

if sum(x) < 50:
  print("EUREKA")
else:
  print("este es el último taller")
